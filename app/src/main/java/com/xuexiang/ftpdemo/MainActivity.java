package com.xuexiang.ftpdemo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.xuexiang.ftp.FtpClient;
import com.xuexiang.xaop.annotation.Permission;
import com.xuexiang.xutil.app.ActivityUtils;
import com.xuexiang.xutil.app.IntentUtils;
import com.xuexiang.xutil.app.PathUtils;
import com.xuexiang.xutil.file.FileUtils;
import com.xuexiang.xutil.system.AppExecutors;
import com.xuexiang.xutil.tip.ToastUtils;

import java.io.File;

import static com.xuexiang.xaop.consts.PermissionConsts.STORAGE;

public class MainActivity extends AppCompatActivity {

    FtpClient ftpClient;
    private static final int REQUEST_CODE_CHOOSE_FILE = 20;
    private static final String IP_ADDRESS = "192.168.115.144";
    private static final String LOGIN_NAME = "test";
    private static final String PASSWORD = "123";

    TextView tvUploadFilePath;
    EditText etDownloadFilePath;

    boolean isSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ftpClient = new FtpClient();

        tvUploadFilePath = findViewById(R.id.tv_upload_file_path);

        etDownloadFilePath = findViewById(R.id.et_download_file_path);

    }


    public void onClick(View view) {
        boolean result;
        switch(view.getId()) {
            case R.id.btn_connect:
                result = ftpClient.connect(IP_ADDRESS);
                ToastUtils.toast("连接" + (result ? "成功" : "失败"));
                break;
            case R.id.btn_login:
                result = ftpClient.login(LOGIN_NAME, PASSWORD);
                ToastUtils.toast("登陆" + (result ? "成功" : "失败"));
                break;
            case R.id.btn_connect_login:
                result = ftpClient.login(IP_ADDRESS, LOGIN_NAME, PASSWORD);
                ToastUtils.toast("登陆" + (result ? "成功" : "失败"));
                break;
            case R.id.btn_quit:
                result = ftpClient.quit();
                ToastUtils.toast("登出" + (result ? "成功" : "失败"));
                break;
            case R.id.btn_download:
                download();
                break;
            case R.id.btn_choose:
                chooseFile();
                break;
            case R.id.btn_upload:
                uploadFile();
            default:
                break;
        }
    }

    private void uploadFile() {
        if (!isSelected) {
            ToastUtils.toast("请先选择文件！");
            return;
        }
        AppExecutors.get().networkIO().execute(new Runnable() {
            @Override
            public void run() {
                boolean result = ftpClient.upload(tvUploadFilePath.getText().toString(), "");
                ToastUtils.toast("文件上传" + (result ? "成功" : "失败"));
            }
        });

    }

    @Permission(STORAGE)
    private void download() {
        final String targetPath = etDownloadFilePath.getText().toString();
        AppExecutors.get().networkIO().execute(new Runnable() {
            @Override
            public void run() {
                boolean result = ftpClient.download(targetPath, PathUtils.getExtDownloadsPath() + File.separator);
                ToastUtils.toast("文件下载" + (result ? "成功" : "失败"));
            }
        });
    }

    @Permission(STORAGE)
    private void chooseFile() {
        ActivityUtils.startActivityForResult(this, IntentUtils.getDocumentPickerIntent(IntentUtils.DocumentType.ANY), REQUEST_CODE_CHOOSE_FILE);
    }

    @Override
    @SuppressLint("MissingPermission")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK || requestCode == REQUEST_CODE_CHOOSE_FILE) {
            if (data != null) {
                String path = PathUtils.getFilePathByUri(data.getData());
                tvUploadFilePath.setText(path);
                isSelected = true;
            }
        }
    }
}
