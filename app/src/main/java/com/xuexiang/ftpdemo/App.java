package com.xuexiang.ftpdemo;

import android.app.Application;

import com.xuexiang.xaop.XAOP;
import com.xuexiang.xutil.XUtil;

/**
 * @author xuexiang
 * @since 2018/11/20 下午5:02
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        XUtil.init(this);
        XAOP.init(this);
    }
}
